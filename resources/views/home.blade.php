@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default text-center" >
				<div class="panel-heading">Bienvenido(a)</div>


        <h1>Tablas a llenar</h1>
        <!-- List group -->
        <ul class="list-group">
          
          <li class="list-group-item">
            <a href="{{url('nivelescolar')}}"> Niveles escolares  </a>
          </li>
          <li class="list-group-item">
            <a href="{{url('ciclo')}}"> Ciclos  </a>
          </li>
          <li class="list-group-item">
            <a href="{{url('grado')}}"> Grados  </a>
          </li>          

          <li class="list-group-item">
            <a href="{{url('area')}}"> Asignaturas </a>
          </li>  

          <li class="list-group-item">
            <a href="{{url('estandar')}}"> Estandares  </a>
          </li>

          <li class="list-group-item">
            <a href="{{url('periodo')}}"> Periodos  </a>
          </li>

          <li class="list-group-item">
            <a href="{{url('logro')}}"> Logros  </a>
          </li>


          <li class="list-group-item">
             Temas (<span style="color:red;">Proximamente disponible</span>)
          </li>

          <li class="list-group-item">
             Fases (<span style="color:red;">Proximamente disponible</span>)
          </li>

          <li class="list-group-item">
             Niveles (<span style="color:red;">Proximamente disponible</span>)
          </li>          


          <li class="list-group-item">
             Matrices (<span style="color:red;">Proximamente disponible</span>)
          </li>
        </ul>

			</div>
		</div>
	</div>
</div>
@endsection
