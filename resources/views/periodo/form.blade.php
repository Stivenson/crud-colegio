@extends('app')

@section('content')

<?php

$urlform = url('periodo');
$enabledprimaria = '';
$verbo = 'POST';
$disabled = '';
$name = '';
$estandar_id = null;


if(isset($item)){

  $urlform = url('periodo').'/'.$item->id;
  $enabledprimaria = 'readonly';
  $verbo = 'PUT';
  $name=$item->name;
  $estandar_id = $item->estandar_id;
  
}

?>



<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="panel panel-default">
        <div class="panel-heading">Periodo</div>
        <div class="panel-body">

          <form method="POST" action="{{$urlform}}" accept-charset="UTF-8" id="formitem" class="form-horizontal" enctype="multipart/form-data">

            <input name="_token" type="hidden" value="{!! csrf_token() !!}" />

            @if($verbo == 'PUT')
              <input name="_method" type="hidden" value="PUT">
            @endif

            <!-- Text input-->
            <div class="form-group">
              <label class="col-md-4 control-label" for="nombre">Nombre</label>  
              <div class="col-md-6"> 
                <input id="name" value="{{$name}}" name="name" type="text" placeholder="" class="form-control input-md" required="">              
              </div>
            </div>


            <!-- Text input-->
            <div class="form-group">
              <label class="col-md-4 control-label" for="estandar_id">Estandar</label>  
              <div class="col-md-6">
                {!! Form::select('estandar_id', $estandares, $estandar_id, array('class'=> 'form-control input-md','required'=>'')) !!}   
              </div>
            </div>



            <div class="text-center">
              <input class="btn btn-primary" type="submit" value="Guardar" />
            </div>
          </form>

        </div>  
      </div>
    </div>
  </div>  
</div>

@stop
