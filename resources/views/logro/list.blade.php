@extends('app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="panel panel-default">
        <div class="panel-heading">Logros</div>

        <div class="panel-body">



          <a href="{{url('logro/create')}}" class="btn btn-link">Nuevo</a>

          @if(count($items) < 1)

          <div class="alert alert-info" role="alert" >
            En el momento no hay registros
          </div>

          @else

          <div class="table-responsive">
          <table class="table table-striped ">
              <tr>
                  <th colspan="2">Acciones</th>
                  <th>Contenido</th>
                  <th>Periodo</th>
                  <th>Estandar</th>                  
              </tr>

              @foreach ($items as $item)             

              <tr>               
                  <td width="50"> 
                    <a href="{{url('logro/'.$item->id.'/edit')}}" class="btn btn-link">Editar</a>               
                  </td>
                  <td> 
                    <form  class="formdelete"  method="POST" action="{{url('logro/'.$item->id)}}" accept-charset="UTF-8" role="form">
                      <input name="_token" type="hidden" value="{!! csrf_token() !!}" />
                      <input name="_method" type="hidden" value="DELETE">
                      <input class="btn btn-link destroy-link" type="submit" value=" Eliminar"> 
                    </form>
                  </td>
                  <td>{{$item->name}}</td>
                  <td>{{$item->periodo->name}}</td>  
                  <td>{{$item->periodo->estandar->name}}</td>               
                  
              </tr>
           @endforeach
          </table>
          </div>


          <center>
              {!!$items->render()!!} 
          </center>


          @endif



        </div>
      </div>
    </div>
  </div>
</div>
@endsection
