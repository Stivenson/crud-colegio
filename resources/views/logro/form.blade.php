@extends('app')

@section('content')

<?php

$urlform = url('logro');
$enabledprimaria = '';
$verbo = 'POST';
$disabled = '';
$name = '';
$periodo_id = null;



if(isset($item)){

  $urlform = url('logro').'/'.$item->id;
  $enabledprimaria = 'readonly';
  $verbo = 'PUT';
  $name=$item->name;
  $periodo_id = $item->periodo_id;


}

?>



<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="panel panel-default">
        <div class="panel-heading">Logro</div>
        <div class="panel-body">

          <form method="POST" action="{{$urlform}}" accept-charset="UTF-8" id="formitem" class="form-horizontal" enctype="multipart/form-data">

            <input name="_token" type="hidden" value="{!! csrf_token() !!}" />

            @if($verbo == 'PUT')
              <input name="_method" type="hidden" value="PUT">
            @endif

            <!-- Text input-->
            <div class="form-group">
              <label class="col-md-4 control-label" for="name">Contenido</label>  
              <div class="col-md-6">
                <textarea required class="form-control" id="name" name="name" rows="5">{{$name}}</textarea>            
              </div>
            </div>


            <!-- Text input-->
            <div class="form-group">
              <label class="col-md-4 control-label" for="periodo_id">Periodo</label>  
              <div class="col-md-6">
                {!! Form::select('periodo_id', $periodos, $periodo_id, array('class'=> 'form-control input-md','required'=>'')) !!}   
              </div>
            </div>


            <div class="text-center">
              <input class="btn btn-primary" type="submit" value="Guardar" />
            </div>
          </form>

        </div>  
      </div>
    </div>
  </div>  
</div>

@stop
