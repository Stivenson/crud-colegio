@extends('app')

@section('content')

<?php

$urlform = url('estandar');
$enabledprimaria = '';
$verbo = 'POST';
$disabled = '';
$name = '';
$area_id = null;


if(isset($item)){

  $urlform = url('estandar').'/'.$item->id;
  $enabledprimaria = 'readonly';
  $verbo = 'PUT';
  $name=$item->name;
  $area_id = $item->area_id;
  
}

?>



<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="panel panel-default">
        <div class="panel-heading">Estandar</div>
        <div class="panel-body">

          <form method="POST" action="{{$urlform}}" accept-charset="UTF-8" id="formitem" class="form-horizontal" enctype="multipart/form-data">

            <input name="_token" type="hidden" value="{!! csrf_token() !!}" />

            @if($verbo == 'PUT')
              <input name="_method" type="hidden" value="PUT">
            @endif

            <!-- Text input-->
            <div class="form-group">
              <label class="col-md-4 control-label" for="nombre">Contenido</label>  
              <div class="col-md-6"> 
                <textarea required class="form-control" id="name" name="name" rows="5">{{$name}}</textarea>            
              </div>
            </div>


            <!-- Text input-->
            <div class="form-group">
              <label class="col-md-4 control-label" for="area_id">Asignatura</label>  
              <div class="col-md-6">
                {!! Form::select('area_id', $areas, $area_id, array('class'=> 'form-control input-md','required'=>'')) !!}   
              </div>
            </div>



            <div class="text-center">
              <input class="btn btn-primary" type="submit" value="Guardar" />
            </div>
          </form>

        </div>  
      </div>
    </div>
  </div>  
</div>

@stop
