<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Siaes-Matematicas</title>

	<link href="{{URL::asset('css/app.css')}}" rel="stylesheet">


	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"><strong>Siaes</strong>-Matematicas</a>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					{{--<li><a href="/">Ingreso de datos</a></li>--}}
          <li><a>Ingreso de datos</a></li>
				</ul>

				<ul class="nav navbar-nav navbar-right">
					@if (Auth::guest())
            <?php

              date_default_timezone_set('america/bogota');

               $arrayMeses = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
                'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
 
                $arrayDias = array( 'Domingo', 'Lunes', 'Martes',
                'Miercoles', 'Jueves', 'Viernes', 'Sabado');
     
            ?>

            <li><a>{{$arrayDias[date('w')].", ".date('d')." de ".$arrayMeses[date('m')-1]." de ".date('Y')}}</a></li>
					@else
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="{{url('/auth/logout')}}">Cerrar Sesión</a></li>
							</ul>
						</li>
					@endif
				</ul>
			</div>
		</div>
	</nav>

  @if (Session::has('action') && Session::get('action'))
  <div class="alert alert-success"><strong>La acción solicitada</strong> se realizo con éxito</div>
  @elseif (Session::has('action') && !Session::get('action'))
  <div class="alert alert-danger"><strong>La acción solicitada</strong> NO se pudo realizar</div>
  @endif

	@yield('content')

  <hr />

  @if(!Auth::guest())
    
    <div class="text-center" >
      <a href="{{url('home')}}">Atras</a>
    </div>  
      
  @endif
  <br/>
  <br/>
  <br/>
  <br/>

	<!-- Scripts -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
  <script>
    $(document).ready(function () {
        $('form.formdelete').submit(function() {
            var c = confirm("¿Esta seguro(a)? También se eliminarán todos los datos asociados a este registro");
            return c;
        });
    }); 
  </script>
</body>
</html>
