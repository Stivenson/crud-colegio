<?php namespace SiaesMatematicas;

use Illuminate\Database\Eloquent\Model;


class Estandar extends Model {

	protected $table = 'estandar'; 


  public function area() {
    return $this->belongsTo('SiaesMatematicas\Area');
  }


}
