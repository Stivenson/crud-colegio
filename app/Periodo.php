<?php namespace SiaesMatematicas;

use Illuminate\Database\Eloquent\Model;


class Periodo extends Model {

	protected $table = 'periodo'; 


  public function estandar() {
    return $this->belongsTo('SiaesMatematicas\Estandar');
  }


}
