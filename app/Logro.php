<?php namespace SiaesMatematicas;

use Illuminate\Database\Eloquent\Model;


class Logro extends Model {

	protected $table = 'logro'; 


  public function periodo() {
    return $this->belongsTo('SiaesMatematicas\Periodo');
  }


}
