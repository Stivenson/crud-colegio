<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

Route::group(['middleware' => 'auth'], function()
{
  
  Route::get('home', 'HomeController@index');
  Route::resource('nivelescolar', 'NivelescolarController',array('except' => array('show')));
  Route::resource('ciclo', 'CicloController',array('except' => array('show')));
  Route::resource('grado', 'GradoController',array('except' => array('show')));
  Route::resource('area', 'AreaController',array('except' => array('show')));
  Route::resource('logro', 'LogroController',array('except' => array('show')));
  Route::resource('estandar', 'EstandarController',array('except' => array('show')));
  Route::resource('periodo', 'PeriodoController',array('except' => array('show')));


});

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);



