<?php namespace SiaesMatematicas\Http\Controllers;

use SiaesMatematicas\Http\Requests;
use SiaesMatematicas\Http\Controllers\Controller;
use SiaesMatematicas\Estandar;
use SiaesMatematicas\Periodo;
use DB;

 

class PeriodoController extends Controller {

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    $items = Periodo::orderBy('created_at','ASC')->paginate(1000);
    return view('periodo/list',array('items'=>$items));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {

    $estandares = array('' => 'Seleccione...') + Estandar::leftJoin('area', 'area.id', '=', 'estandar.area_id')
    ->select(array(DB::raw('CONCAT("Estandar: ",SUBSTRING(estandar.name,1,100),"...   Asignatura: ",area.name) as estandararea'),'estandar.id as estandar_id'))
    ->lists('estandararea', 'estandar_id');

    return view('periodo/form',array('estandares'=>$estandares));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store()
  {
    try{

      $inputs = $this->request->all();

      $obj = new Periodo();

      $obj->name = $inputs['name'];
      $obj->estandar_id = $inputs['estandar_id'];

      $obj->save();

      return redirect('periodo')->with('action',true);
                          
    }catch(Exception $e){

      return redirect('periodo')->with('action',false);

    }

  }



  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    $item = Periodo::find($id);

    $estandares = array('' => 'Seleccione...') + Estandar::leftJoin('area', 'area.id', '=', 'estandar.area_id')
    ->select(array(DB::raw('CONCAT("Estandar: ",SUBSTRING(estandar.name,1,100),"...   Asignatura: ",area.name) as estandararea'),'estandar.id as estandar_id'))
    ->lists('estandararea', 'estandar_id');


    return view('periodo/form',array('item'=>$item,'estandares'=>$estandares));

  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {
    try{

      $inputs = $this->request->all();

      $obj = Periodo::find($id);

      $obj->name = $inputs['name'];
      $obj->estandar_id = $inputs['estandar_id'];
      
      $obj->save();

      return redirect('periodo')->with('action',true);
                          
    }catch(Exception $e){

      return redirect('periodo')->with('action',false);

    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
  
    try{

      $obj = Periodo::find($id);

      $obj->delete();

      return redirect('periodo')->with('action',true);
                          
    }catch(Exception $e){

      return redirect('periodo')->with('action',false);

    }
  
  }

}
