<?php namespace SiaesMatematicas\Http\Controllers;

use SiaesMatematicas\Http\Requests;
use SiaesMatematicas\Http\Controllers\Controller;
use SiaesMatematicas\Area;
use SiaesMatematicas\Grado;

 

class AreaController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$items = Area::orderBy('created_at','ASC')->paginate(1000);
    return view('area/list',array('items'=>$items));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
    $grados = array('' => 'Seleccione...') + Grado::lists('name','id');

    return view('area/form',array('grados'=>$grados));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
    try{

      $inputs = $this->request->all();

      $obj = new Area();

      $obj->name = $inputs['name'];
      $obj->grado_id = $inputs['grado_id'];


      $obj->save();

      return redirect('area')->with('action',true);
                          
    }catch(Exception $e){

      return redirect('area')->with('action',false);

    }

	}



	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$item = Area::find($id);
    $grados = array('' => 'Seleccione...') + Grado::lists('name','id');

    return view('area/form',array('item'=>$item,'grados'=>$grados));

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
    try{

      $inputs = $this->request->all();

      $obj = Area::find($id);

      $obj->name = $inputs['name'];
      $obj->grado_id = $inputs['grado_id'];
      
      $obj->save();

      return redirect('area')->with('action',true);
                          
    }catch(Exception $e){

      return redirect('area')->with('action',false);

    }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
  
    try{

      $obj = Area::find($id);

      $obj->delete();

      return redirect('area')->with('action',true);
                          
    }catch(Exception $e){

      return redirect('area')->with('action',false);

    }
	
  }

}
