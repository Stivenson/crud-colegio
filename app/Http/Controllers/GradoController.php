<?php namespace SiaesMatematicas\Http\Controllers;

use SiaesMatematicas\Http\Requests;
use SiaesMatematicas\Http\Controllers\Controller;
use SiaesMatematicas\Grado;
use SiaesMatematicas\Ciclo;

 

class GradoController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$items = Grado::orderBy('created_at','ASC')->paginate(1000);
    return view('grado/list',array('items'=>$items));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
    $ciclos = array('' => 'Seleccione...') + Ciclo::lists('name','id');

    return view('grado/form',array('ciclos'=>$ciclos));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
    try{

      $inputs = $this->request->all();

      $obj = new Grado();

      $obj->name = $inputs['name'];
      $obj->ciclo_id = $inputs['ciclo_id'];

      $obj->save();

      return redirect('grado')->with('action',true);
                          
    }catch(Exception $e){

      return redirect('grado')->with('action',false);

    }

	}



	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$item = Grado::find($id);
    $ciclos = array('' => 'Seleccione...') + Ciclo::lists('name','id');

    return view('grado/form',array('item'=>$item,'ciclos'=>$ciclos));

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
    try{

      $inputs = $this->request->all();

      $obj = Grado::find($id);

      $obj->name = $inputs['name'];
      $obj->ciclo_id = $inputs['ciclo_id'];
      
      $obj->save();

      return redirect('grado')->with('action',true);
                          
    }catch(Exception $e){

      return redirect('grado')->with('action',false);

    }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
  
    try{

      $obj = Grado::find($id);

      $obj->delete();

      return redirect('grado')->with('action',true);
                          
    }catch(Exception $e){

      return redirect('grado')->with('action',false);

    }
	
  }

}
