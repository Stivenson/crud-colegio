<?php namespace SiaesMatematicas\Http\Controllers;

use SiaesMatematicas\Http\Requests;
use SiaesMatematicas\Http\Controllers\Controller;
use SiaesMatematicas\Estandar;
use SiaesMatematicas\Area;
use DB;

 

class EstandarController extends Controller {

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    $items = Estandar::orderBy('created_at','ASC')->paginate(1000);
    return view('estandar/list',array('items'=>$items));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {

    $areas = array('' => 'Seleccione...') + Area::leftJoin('grado', 'grado.id', '=', 'area.grado_id')
    ->select(array(DB::raw('CONCAT("Asignatura: ",area.name," --- Grado: ",grado.name) as areagrado'),'area.id as area_id'))
    ->lists('areagrado', 'area_id');

    return view('estandar/form',array('areas'=>$areas));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store()
  {
    try{

      $inputs = $this->request->all();

      $obj = new Estandar();

      $obj->name = $inputs['name'];
      $obj->area_id = $inputs['area_id'];


      $obj->save();

      return redirect('estandar')->with('action',true);
                          
    }catch(Exception $e){

      return redirect('estandar')->with('action',false);

    }

  }



  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    $item = Estandar::find($id);
    $areas = array('' => 'Seleccione...') + Area::leftJoin('grado', 'grado.id', '=', 'area.grado_id')
    ->select(array(DB::raw('CONCAT("Asignatura: ",area.name," --- Grado: ",grado.name) as areagrado'),'area.id as area_id'))
    ->lists('areagrado', 'area_id');


    return view('estandar/form',array('item'=>$item,'areas'=>$areas));

  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {
    try{

      $inputs = $this->request->all();

      $obj = Estandar::find($id);

      $obj->name = $inputs['name'];
      $obj->area_id = $inputs['area_id'];
      
      $obj->save();

      return redirect('estandar')->with('action',true);
                          
    }catch(Exception $e){

      return redirect('estandar')->with('action',false);

    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
  
    try{

      $obj = Estandar::find($id);

      $obj->delete();

      return redirect('estandar')->with('action',true);
                          
    }catch(Exception $e){

      return redirect('estandar')->with('action',false);

    }
  
  }

}
