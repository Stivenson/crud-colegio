<?php namespace SiaesMatematicas\Http\Controllers;

use SiaesMatematicas\Http\Requests;
use SiaesMatematicas\Http\Controllers\Controller;
use SiaesMatematicas\Logro;
use SiaesMatematicas\Periodo;
use DB;

 

class LogroController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$items = Logro::orderBy('created_at','ASC')->paginate(1000);
    return view('logro/list',array('items'=>$items));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{

    $periodos = array('' => 'Seleccione...') + Periodo::leftJoin('estandar', 'estandar.id', '=', 'periodo.estandar_id')
    ->select(array(DB::raw('CONCAT("Periodo: ",periodo.name," ... Estandar: ",SUBSTRING(estandar.name,1,100),"...") as periodoestandar'),'periodo.id as periodo_id'))
    ->lists('periodoestandar', 'periodo_id');

    return view('logro/form',array('periodos'=>$periodos));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
    try{

      $inputs = $this->request->all();

      $obj = new Logro();

      $obj->name = $inputs['name'];
      $obj->periodo_id = $inputs['periodo_id'];


      $obj->save();

      return redirect('logro')->with('action',true);
                          
    }catch(Exception $e){

      return redirect('logro')->with('action',false);

    }

	}



	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$item = Logro::find($id);
    $periodos = array('' => 'Seleccione...') + Periodo::leftJoin('estandar', 'estandar.id', '=', 'periodo.estandar_id')
    ->select(array(DB::raw('CONCAT("Periodo: ",periodo.name," ... Estandar: ",SUBSTRING(estandar.name,1,100),"...") as periodoestandar'),'periodo.id as periodo_id'))
    ->lists('periodoestandar', 'periodo_id');

    return view('logro/form',array('item'=>$item,'periodos'=>$periodos));

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
    try{

      $inputs = $this->request->all();

      $obj = Logro::find($id);

      $obj->name = $inputs['name'];
      $obj->periodo_id = $inputs['periodo_id'];
      
      $obj->save();

      return redirect('logro')->with('action',true);
                          
    }catch(Exception $e){

      return redirect('logro')->with('action',false);

    }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
  
    try{

      $obj = Logro::find($id);

      $obj->delete();

      return redirect('logro')->with('action',true);
                          
    }catch(Exception $e){

      return redirect('logro')->with('action',false);

    }
	
  }

}
