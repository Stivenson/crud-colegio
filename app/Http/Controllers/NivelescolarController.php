<?php namespace SiaesMatematicas\Http\Controllers;

use SiaesMatematicas\Http\Requests;
use SiaesMatematicas\Http\Controllers\Controller;
use SiaesMatematicas\Nivelescolar;
 

class NivelescolarController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$items = Nivelescolar::orderBy('created_at','ASC')->paginate(1000);
    return view('nivelescolar/list',array('items'=>$items));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
    return view('nivelescolar/form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
    try{

      $inputs = $this->request->all();

      $obj = new Nivelescolar();

      $obj->name = $inputs['name'];

      $obj->save();

      return redirect('nivelescolar')->with('action',true);
                          
    }catch(Exception $e){

      return redirect('nivelescolar')->with('action',false);

    }

	}



	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$item = Nivelescolar::find($id);
    return view('nivelescolar/form',array('item'=>$item));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
    try{

      $inputs = $this->request->all();

      $obj = Nivelescolar::find($id);

      $obj->name = $inputs['name'];

      $obj->save();

      return redirect('nivelescolar')->with('action',true);
                          
    }catch(Exception $e){

      return redirect('nivelescolar')->with('action',false);

    }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
  
    try{

      $obj = Nivelescolar::find($id);

      $obj->delete();

      return redirect('nivelescolar')->with('action',true);
                          
    }catch(Exception $e){

      return redirect('nivelescolar')->with('action',false);

    }
	
  }

}
