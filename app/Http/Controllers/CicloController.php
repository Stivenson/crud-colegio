<?php namespace SiaesMatematicas\Http\Controllers;

use SiaesMatematicas\Http\Requests;
use SiaesMatematicas\Http\Controllers\Controller;
use SiaesMatematicas\Ciclo;
use SiaesMatematicas\Nivelescolar; 

class CicloController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$items = Ciclo::orderBy('created_at','ASC')->paginate(1000);
    return view('ciclo/list',array('items'=>$items));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
    $nivelesescolares = array('' => 'Seleccione...') + Nivelescolar::lists('name','id');    
    return view('ciclo/form',array('nivelesescolares'=>$nivelesescolares));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
    try{

      $inputs = $this->request->all();

      $obj = new Ciclo();

      $obj->name = $inputs['name'];
      $obj->nivel_escolar_id = $inputs['nivel_escolar_id'];

      $obj->save();

      return redirect('ciclo')->with('action',true);
                          
    }catch(Exception $e){

      return redirect('ciclo')->with('action',false);

    }

	}



	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$item = Ciclo::find($id);
    $nivelesescolares = array('' => 'Seleccione...') + Nivelescolar::lists('name','id'); 
    return view('ciclo/form',array('item'=>$item,'nivelesescolares'=>$nivelesescolares));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
    try{

      $inputs = $this->request->all();

      $obj = Ciclo::find($id);

      $obj->name = $inputs['name'];

      $obj->nivel_escolar_id = $inputs['nivel_escolar_id'];

      $obj->save();

      return redirect('ciclo')->with('action',true);
                          
    }catch(Exception $e){

      return redirect('ciclo')->with('action',false);

    }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
  
    try{

      $obj = Ciclo::find($id);

      $obj->delete();

      return redirect('ciclo')->with('action',true);
                          
    }catch(Exception $e){

      return redirect('ciclo')->with('action',false);

    }
	
  }

}
