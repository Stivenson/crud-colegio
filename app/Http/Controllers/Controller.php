<?php namespace SiaesMatematicas\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

abstract class Controller extends BaseController {

	use DispatchesCommands, ValidatesRequests;
  protected $request;

  public function __construct(\Illuminate\Http\Request $request)
  {
       $this->request = $request;
  }

}
