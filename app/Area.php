<?php namespace SiaesMatematicas;

use Illuminate\Database\Eloquent\Model;


class Area extends Model {

	protected $table = 'area'; 


  public function grado() {
    return $this->belongsTo('SiaesMatematicas\Grado');
  }


}
